# Workshop
## Minikube dashboard
Dashboard is een webgebaseerde Kubernetes-gebruikersinterface. U kunt Dashboard gebruiken om containertoepassingen in een Kubernetes-cluster te implementeren, problemen met uw containertoepassing op te lossen en de clusterbronnen te beheren. U kunt Dashboard gebruiken om een overzicht te krijgen van toepassingen die op uw cluster worden uitgevoerd, evenals voor het maken of wijzigen van afzonderlijke Kubernetes-bronnen (zoals implementaties, taken, DaemonSets, enz.). U kunt bijvoorbeeld een implementatie schalen, een rolling update initiëren, een pod opnieuw opstarten of nieuwe toepassingen implementeren met behulp van een implementatiewizard.
```bash
minikube dashboard
```

## Deployments
*	[Deployment of applications](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
*	[Rolling update](rolling-update.md)

## Configure Pods and Containers
*	[Assign Memory Resources to Containers and Pods](https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/)
*	[Assign CPU Resources to Containers and Pods](https://kubernetes.io/docs/tasks/configure-pod-container/assign-cpu-resource/)
*	[Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)

## Inject data in applications
*	[Configure a Pod to Use a ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/)
* 	[Define a Command and Arguments for a Container](https://kubernetes.io/docs/tasks/inject-data-application/define-command-argument-container/)
* 	[Define Environment Variables for a Container](https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/)
*	[Expose Pod Information to Containers Through Environment Variables](https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/)
*	[Expose Pod Information to Containers Through Files](https://kubernetes.io/docs/tasks/inject-data-application/downward-api-volume-expose-pod-information/)
*	[Distribute Credentials Securely Using Secrets](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/)
*	Start minikube als volgt op om PodPreset te laten werken
```bash
minikube start --extra-config=apiserver.runtime-config=settings.k8s.io/v1alpha1=true --extra-config=apiserver.enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,NodeRestriction,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota,PodPreset
```
*	[Inject Information into Pods Using a PodPreset](https://kubernetes.io/docs/tasks/inject-data-application/podpreset/)
*	[Communicate Between Containers in the Same Pod Using a Shared Volume](https://kubernetes.io/docs/tasks/access-application-cluster/communicate-containers-same-pod-shared-volume/)

## Service en Ingress
*	[Use a Service to Access an Application in a Cluster](https://kubernetes.io/docs/tasks/access-application-cluster/service-access-application-cluster/)
*	[Connect a Front End to a Back End Using a Service](https://kubernetes.io/docs/tasks/access-application-cluster/connecting-frontend-backend/)
*	[Set up Ingress on Minikube with the NGINX Ingress Controller](https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/)
	


