# Techmeeting Kubernetes 20 februari 2020

## Workshop 20 februari 2020

## Voorbereiding
1. Installeer Oracle Virtual Box
* https://download.virtualbox.org/virtualbox/6.1.2/VirtualBox-6.1.2-135663-Win.exe
	
2. Instaleer kubectl
* https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-windows
* Download https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/windows/amd64/kubectl.exe
* Controleer kubectl
```bash
$ kubectl version --client
Client Version: version.Info{Major:"1", Minor:"17", GitVersion:"v1.17.0", GitCommit:"70132b0f130acc0bed193d9ba59dd186f0e634cf", GitTreeState:"clean", BuildDate:"2019-12-07T21:20:10Z", GoVersion:"go1.13.4", Compiler:"gc", Platform:"windows/amd64"}
```	

3. Installateer minikube
* https://kubernetes.io/docs/tasks/tools/install-minikube/
* Download https://github.com/kubernetes/minikube/releases/latest/download/minikube-installer.exe
* Controleer installatie minikube
* Controleer installatie minikube
```bash
$ minikube version
minikube version: v1.6.2
commit: 54f28ac5d3a815d1196cd5d57d707439ee4bb392
```
* Start minikube
```bash
$ minikube start
* minikube v1.6.2 on Microsoft Windows 10 Home 10.0.18362 Build 18362
* Automatically selected the 'virtualbox' driver (alternates: [])
* Creating virtualbox VM (CPUs=2, Memory=2000MB, Disk=20000MB) ...
* Preparing Kubernetes v1.17.0 on Docker '19.03.5' ...
* Pulling images ...
* Launching Kubernetes ...
* Waiting for cluster to come online ...
* Done! kubectl is now configured to use "minikube"
```
* Stop minikube
```bash
$ minikube stop
* Stopping "minikube" in virtualbox ...
* "minikube" stopped.
```	
* Delete minikube in virtualbox
```bash
$ minikube delete
* Deleting "minikube" in virtualbox ...
* The "minikube" cluster has been deleted.
* Successfully deleted profile "minikube"
```
## Workshop minikube
[Workshop minikube](workshop-minikube.md "Workshop") 
